package Projectpkg;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TCFlipkart01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		Actions builder = new Actions(driver);
		WebElement electornics = driver.findElementByXPath("//span[text()='Electronics']");
		builder.moveToElement(electornics).perform();
		driver.findElementByLinkText("Mi").click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.titleContains("Mi"));
		String miTitle = driver.getTitle();
		System.out.println(miTitle);
		if((miTitle).contains("Mi Mobile"))
			{
				System.out.println("The window title is" +miTitle);
			}
		else
			{
			System.out.println("The window title is not verified");
			}
		
		driver.findElementByXPath("//div[text()='Newest First']").click();
		//driver.findElementByLinkText("Newest First").click();
		List<WebElement> getProductName = driver.findElementsByXPath("//div[@class='col col-7-12']/div[1]");
		//System.out.println(getProductName);
		
		List<WebElement> getPrice = driver.findElementsByXPath("//div[@class='col col-5-12 _2o7WAb']/div[1]");
		//System.out.println(getPrice);
		for(int i=0;i<getProductName.size();i++)
		{
			System.out.println(getProductName.get(i).getText()+" "+getPrice.get(i).getText().replaceAll("\\d", ""));
		}
		driver.findElementByXPath("//div[text()='Redmi 6 Pro (Black, 32 GB)']").click();
		Set<String> windows = driver.getWindowHandles();
		List<String> lst= new ArrayList<>();
		lst.addAll(windows);
		driver.switchTo().window(lst.get(1));
		System.out.println(driver.getTitle());
		String getRatings = driver.findElementByXPath("//span[contains(text(),'Ratings')]").getText();
			System.out.println(getRatings);
			String getReview = driver.findElementByXPath("//span[contains(text(),'Reviews')]").getText();
			System.out.println(getReview);
		
		
	}

}
