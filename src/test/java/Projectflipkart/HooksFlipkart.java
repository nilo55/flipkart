package Projectflipkart;

import cucumber.api.java.After;
import cucumber.api.java.Before;

import wdMethods.SeMethods;

public class HooksFlipkart extends SeMethods {
	@Before
	public void callMethodBeforeFlipkart()
	{
		
		startResult();
		startTestModule("FlipkartTcWithCucumber", "To view flipkart products");
		startTestCase("Products");
		test = startTestCase("Products");
		test.assignCategory("Smoke");
		test.assignAuthor("Saranya");
		startApp("chrome", "https://www.flipkart.com/");	
		
	}
	@After
	public void callMethodAfterFlipkart()
	{
		closeAllBrowsers();
		endResult();
	}
}
