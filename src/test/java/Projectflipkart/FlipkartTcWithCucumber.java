package Projectflipkart;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.SeMethods;

public class FlipkartTcWithCucumber extends SeMethods {
	public ChromeDriver driver;


	@Given("Hover Electronics")
	public void hoverElectronics() {
		KeyboardActions();
	}

	@When("Click Mi")
	public void clickMi() {
	
		WebElement electronics = locateElement("xpath", "//span[text()='Electronics']");		
		mouseHover(electronics);
		WebElement elegetmiLink = locateElement("link", "Mi");
		click(elegetmiLink);
	}

	@Then("Verify the Window Title")
	public void verifyTheWindowTitle() {
		//WebDriverWait wait = new WebDriverWait(driver, 10);
		//wait.until(ExpectedConditions.titleContains("Mi"));
		verifyTitle("Mi");
	}

	@Then("Click Newest First")
	public void clickNewestFirst() {
		WebElement eleClickNewFirst = locateElement("xpath", "//div[text()='Newest First']");
		click(eleClickNewFirst);
	}

	@Then("To print all the product name along with price name")
	public void toPrintAllTheProductNameAlongWithPriceName() {
		List<WebElement> getProductName = locateElements("xpath","//div[@class='col col-7-12']/div[1]");
		List<WebElement> getPrice = locateElements("xpath", "//div[@class='col col-5-12 _2o7WAb']/div[1]");
		for(int i=0;i<getProductName.size();i++)
		{
			System.out.println(getProductName.get(i).getText()+" "+getPrice.get(i).getText().replaceAll("\\D", ""));
		}
		
	}

	@When("Click on the first product")
	public void clickOnTheFirstProduct() {
		WebElement eleClickFirstProd = locateElement("xpath","(//div[@class='col col-7-12']/div[1])[1]");
		click(eleClickFirstProd);
	}

	@Then("Print the review and ratings")
	public void printTheReviewAndRatings() {
		switchToWindow(1);
		verifyTitle("Redmi");
		WebElement Ratings = locateElement("xpath", "//span[contains(text(),'Ratings')]");
		System.out.println(getText(Ratings));
		
		WebElement Review = locateElement("xpath", "//span[contains(text(),'Reviews')]");
		System.out.println(getText(Review));
	}

	
	




}
