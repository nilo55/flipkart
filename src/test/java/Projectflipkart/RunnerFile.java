package Projectflipkart;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features ="src\\test\\java\\Projectflipkart\\Flipkart.feature", glue="Projectflipkart", snippets=SnippetType.CAMELCASE,monochrome=true )
public class RunnerFile {

}
